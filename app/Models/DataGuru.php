<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataGuru extends Model
{
    protected $table = 'dataguru';
    
    protected $fillable = ['nama', 'matapelajaran'];
}
