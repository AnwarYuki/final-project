<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataGuru;
use DB;

class DataguruController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataguru = DataGuru::all();
        return view('dataguru.index', compact('dataguru'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dataguru.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'matapelajaran' => 'required',
        ]);
        DataGuru::create([
    		'nama' => $request->nama,
    		'matapelajaran' => $request->matapelajaran
            
    	]);
 
    	return redirect('/dataguru');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataguru = DataGuru::find($id);
        return view('dataguru.show', compact('dataguru'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dataguru = DataGuru::find($id);
        return view('dataguru.edit', compact('dataguru'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'matapelajaran' => 'required'
            
        ]);

        $dataguru = DataGuru::find($id);
        $dataguru->nama = $request->nama;
        $dataguru->matapelajaran = $request->matapelajaran;
        
        $dataguru->update();
        return redirect('/dataguru');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataguru = DataGuru::find($id);
        $dataguru->delete();
        return redirect('/dataguru');
    }
    
}
