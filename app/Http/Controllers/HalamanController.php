<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengumuman;
use App\Models\Event;
use App\Models\DataGuru;

class HalamanController extends Controller
{
    public function index()
    {
        $pengumuman = Pengumuman::all();
        return view('halaman.pengumuman', compact('pengumuman'));
    }
    public function event()
    {
        $event = Event::all();
        return view('halaman.event', compact('event'));
    }
    public function dataguru()
    {
        $dataguru = DataGuru::all();
        return view('halaman.dataguru', compact('dataguru'));
    }

    public function contact()
    {
        return view('halaman.contact');
    }

    public function about()
    {
        return view('halaman.about');
    }
    
}
