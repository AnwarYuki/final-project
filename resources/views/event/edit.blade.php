@extends('layouts.master')

@section('judul')
Edit Data 
@endsection
@section('content')
<div>
        <form action="/event/{{$event->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" value="{{$event->judul}}" name="judul" id="title" placeholder="Edit Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi</label>
                <input type="text" class="form-control" value="{{$event->isi}}" name="isi" id="body" placeholder="Edit Isi">
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                
                <div class="form-group">
                <label for="body">Isi</label>
                <input type="date" class="form-control" value="{{$event->tanggal}}" name="tanggal" id="body" placeholder="date">
                @error('tanggal')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection