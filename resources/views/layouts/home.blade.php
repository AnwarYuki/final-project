<!doctype html>
<html lang="en">

<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>SMKN 1 CISARUA || Dashboard</title>
   <link rel="icon" href="{{ asset('/dreams-master/img/favicon.png')}} ">
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/bootstrap.min.css')}} ">
   <!-- animate CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/animate.css')}} ">
   <!-- owl carousel CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/owl.carousel.min.css')}} ">
   <!-- themify CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/themify-icons.css')}} ">
   <!-- flaticon CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/flaticon.css')}} ">
   <!-- magnific-popup CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/magnific-popup.css')}} ">
   <!-- font awesome CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/fontawesome/css/all.min.css')}} ">
   <!-- style CSS -->
   <link rel="stylesheet" href="{{ asset('/dreams-master/css/style.css')}} ">
</head>

<body>
   <!--::menu part start::-->
   @extends('partials.navhome')
   <!--::menu part end::-->

   @yield('content')
   
   <!--::footer_part start::-->
   @extends('partials.footerhome')
   
   <!--::footer_part end::-->

   <!-- jquery plugins here-->
   <script src="{{ asset('/dreams-master/js/jquery-1.12.1.min.js')}}"></script>
   <!-- popper js -->
   <script src="{{ asset('/dreams-master/js/popper.min.js')}}"></script>
   <!-- bootstrap js -->
   <script src="{{ asset('/dreams-master/js/bootstrap.min.js')}}"></script>
   <!-- magnific js -->
   <script src="{{ asset('/dreams-master/js/jquery.magnific-popup.min.js')}}"></script>
   <!-- carousel js -->
   <script src="{{ asset('/dreams-master/js/owl.carousel.min.js')}}"></script>
   <!-- easing js -->
   <script src="{{ asset('/dreams-master/js/jquery.easing.min.js')}}"></script>
   <!--masonry js-->
   <script src="{{ asset('/dreams-master/js/masonry.pkgd.min.js')}}"></script>
   <script src="{{ asset('/dreams-master/js/masonry.pkgd.js')}}"></script>
   <!--form validation js-->
   <script src="{{ asset('/dreams-master/js/jquery.nice-select.min.js')}}"></script>
	<script src="{{ asset('/dreams-master/js/contact.js')}}"></script>
	<script src="{{ asset('/dreams-master/js/jquery.ajaxchimp.min.js')}}"></script>
	<script src="{{ asset('/dreams-master/js/jquery.form.js')}}"></script>
	<script src="{{ asset('/dreams-master/js/jquery.validate.min.js')}}"></script>
	<script src="{{ asset('/dreams-master/js/mail-script.js')}}"></script>
   <!-- counter js -->
   <script src="{{ asset('/dreams-master/js/jquery.counterup.min.js')}}"></script>
   <script src="{{ asset('/dreams-master/js/waypoints.min.js')}}"></script>
   <!-- custom js -->
   <script src="{{ asset('/dreams-master/js/custom.js')}}"></script>
</body>

</html>