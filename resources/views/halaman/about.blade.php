<@extends('layouts.home')
@section('content')
   <!--::breadcrumb part start::-->



   <section>
   <div class="item active">
        <img src="{{ asset('/dreams-master/img/sekolah_5.jpg')}}" alt="" class="img-responsive" width="1500" height="500">
           <div class="carousel-caption">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="breadcrumb_iner">
                  <div class="breadcrumb_iner_item">
                     <h2>about us</h2>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--::breadcrumb part start::-->


   <!--::team part end::-->
   <section class="about_part section-padding">
      <div class="container">
         <div class="row">
            <div class="section_tittle">
               <h2><span>about</span> us</h2>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6 col-md-6">
               <div class="about_img">
                  <img src="{{ asset('/dreams-master/img/about.png')}}" alt="">
               </div>
            </div>
            <div class="offset-lg-1 col-lg-5 col-sm-8 col-md-6">
               <div class="about_text">
                  <h2>SAMBUTAN KEPALA <span>SEKOLAH</span></h2>
                  <p>SMK Negeri 1 Cisarua Kabupaten Bandung Barat tanggap dengan perkembangan teknologi.
                     Dengan dukungan SDM yang di miliki sekolah ini siap untuk berkompetisi dengan sekolah lain dalam persaingan global. 
                     Teknik Komputer Jaringan, Administrasi Perkantoran, Akomodasi Perhotelan dan Teknik Kendaraan Ringan, 
                     menjadi sarana bagi SMK Negeri 1 Cisarua Kabupaten Bandung Barat siap bersaing di Industri Kerja.
                      Dari layanan ini pula, sekolah siap menerima saran dari semua pihak yang akhirnya dapat menjawab Kebutuhan masyarakat. </p>
                 
                  <div class="about_part_counter">
                     <div class="single_counter">
                        <span class="counter">100</span>
                        <p>Jurusan</p>
                     </div>
                     <div class="single_counter">
                        <span class="counter">300</span>
                        <p>Ruangan</p>
                     </div>
                     <div class="single_counter">
                        <span class="counter">150</span>
                        <p>Guru</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--::team part end::-->

    <!--::team part end::-->
    <section class="project_gallery padding_bottom">
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-12">
               <div class="project_gallery_tittle">
                  <h2><span>Galeri</span> SMKN 1 CISARUA</h2>
               </div>
               <div class="grid">
                  <div class="grid-sizer"></div>
                  <div class="grid-item big_weight">
                     <div class="project_img">
                        <img src="{{ asset('/dreams-master/img/gallery/sekolah_1.jpg')}}" alt="">
                        <div class="project_gallery_hover_text">
                  <h3> Guru - Guru </h3>
                        </div>
                     </div>
                  </div>
                  <div class="grid-item big_height">
                     <div class="project_img">
                        <img src="{{ asset('/dreams-master/img/gallery/sekolah_4.png')}}" alt="">
                        <div class="project_gallery_hover_text">
                           <h3>Siswa </h3>
                        </div>
                     </div>
                  </div>
                  <div class="grid-item">
                     <div class="project_img">
                        <img src="{{ asset('/dreams-master/img/gallery/sekolah_2.png')}}" alt="">
                        <div class="project_gallery_hover_text">
                           <h3>Lapangan</h3>
                        </div>
                     </div>
                  </div>
                  <div class="grid-item">
                     <div class="project_img">
                        <img src="{{ asset('/dreams-master/img/gallery/sekolah_3.png')}}" alt="">
                        <div class="project_gallery_hover_text">
                        <h3>Upacara </h3>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--::team part end::-->

  