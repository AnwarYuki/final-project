@extends('layouts.home')
@section('content')
<!--::breadcrumb part start::-->

<div id="mycarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="item active">
        <img src="{{ asset('/dreams-master/img/sekolah_5.jpg')}}" alt="" class="img-responsive" width="1500" height="500">
           <div class="carousel-caption">
         
      <h1><mark>Data Guru</mark></h1>
         </div>
      </div>
    </div>



        <table class="table">
            <thead class="thead-light">
              <tr>
                
                <th scope="col">Nama Gruru</th>
                <th scope="col">Mata Pelajaran</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($dataguru as $key=>$value)
                    <tr>
                        
                        <td>{{$value->nama}}</td>
                        <td>{{$value->matapelajaran}}</td>
                        <td>
                          
                           
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
                  
             

   <!--::card box end::-->
@endsection