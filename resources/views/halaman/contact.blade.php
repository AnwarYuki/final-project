@extends('layouts.home')
@section('content')
   <!--::menu part end::-->

   <!--::breadcrumb part start::-->
   <section>
   <div class="item active">
        <img src="{{ asset('/dreams-master/img/sekolah_5.jpg')}}" alt="" class="img-responsive" width="1500" height="500">
           <div class="carousel-caption">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="breadcrumb_iner">
                  <div class="breadcrumb_iner_item">
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--::breadcrumb part end::-->

   <!--::team part end::-->

   <!-- ================ contact section start ================= -->
   <div class="col-lg-4">
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-home"></i></span>
            <div class="media-body">
              <h3><a href="">Kabupaten Bandung Barat</a></h3>
              <p>Jalan Kolonel Masturi Desa Jambudipa</p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-tablet"></i></span>
            <div class="media-body">
              <h3>083120371009</h3>
              <p>senin - jumat. jam 08:00 - 15:00 </p>
            </div>
          </div>
          <div class="media contact-info">
            <span class="contact-info__icon"><i class="ti-email"></i></span>
            <div class="media-body">
              <h3><a href="mailto:support@colorlib.com">senibudayasmkn1cisarua@gmail.com</a></h3>
              <p>Kirimkan pertanyaan Anda kapan saja!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- ================ contact section end ================= -->

 
    