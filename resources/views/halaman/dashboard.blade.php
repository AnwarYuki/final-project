@extends('layouts.home')
@section('content')

   <!--::banner part start::-->
   
      
   <div id="mycarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="item active">
        <img src="{{ asset('/dreams-master/img/sekolah_5.jpg')}}" alt="" class="img-responsive" width="1500" height="500">
           <div class="carousel-caption">
         
      <h5><mark>SMKN 1 CISARUA</mark></h5>
      <h2><mark>Kabupaten Bandung Barat </mark> </h2>
      <p><mark>Selamat Datang Di Website SMK Negeri 1 Cisarua</mark> </p>
         </div>
      </div>
    </div>
</div>
   
   <!--::banner part end::-->

   <!--::team part end::-->
   <section class="about_part section-padding">
      <div class="container">
         <div class="row">
            <div class="section_tittle">
               <h2><span>about</span> us</h2>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6 col-md-6">
               <div class="about_img">
                  <img src="{{ asset('/dreams-master/img/about.png')}}" alt="">
               </div>
            </div>
            <div class="offset-lg-1 col-lg-5 col-sm-8 col-md-6">
               <div class="about_text">
                  <h2>SAMBUTAN KEPALA <span>SEKOLAH</span></h2>
                  <p>SMK Negeri 1 Cisarua Kabupaten Bandung Barat tanggap dengan perkembangan teknologi.
                     Dengan dukungan SDM yang di miliki sekolah ini siap untuk berkompetisi dengan sekolah lain dalam persaingan global. 
                     Teknik Komputer Jaringan, Administrasi Perkantoran, Akomodasi Perhotelan dan Teknik Kendaraan Ringan, 
                     menjadi sarana bagi SMK Negeri 1 Cisarua Kabupaten Bandung Barat siap bersaing di Industri Kerja.
                      Dari layanan ini pula, sekolah siap menerima saran dari semua pihak yang akhirnya dapat menjawab Kebutuhan masyarakat. </p>
                 
                  <div class="about_part_counter">
                     <div class="single_counter">
                        <span class="counter">4</span>
                        <p>Jurusan</p>
                     </div>
                     <div class="single_counter">
                        <span class="counter">1500</span>
                        <p>Murid</p>
                     </div>
                     <div class="single_counter">
                        <span class="counter">80</span>
                        <p>Guru</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--::team part end::-->

   <!--::project part start::-->
   <section class="portfolio_area pt_30 padding_bottom">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <div class="section_tittle">
                  <h2><span>our</span> Jurusan</h2>
               </div>
               <div class="portfolio-filter">
                  <h2>Beberapa <br>
                        Jurusan di <span>SMKN 1 CISARUA .</span></h2>
                  <ul class="nav nav-tabs" id="myTab" role="tablist">
                     <li>
                        <a class="active" id="rpltkr-tab" data-toggle="tab" href="#rpltkr" role="tab"
                           aria-controls="rpltkr" aria-selected="true">
                           RPL & TKR
                        </a>
                     </li>
                     <li>
                        <a id="apph-tab" data-toggle="tab" href="#apph" role="tab" aria-controls="apph"
                           aria-selected="false">
                           AP & PH
                        </a>
                     </li>
                  </ul>
               </div>
               <div class="portfolio_item tab-content" id="myTabContent">
                  <div class="row align-items-center justify-content-between tab-pane fade show active"
                     id="rpltkr" role="tabpanel" aria-labelledby="rpltkr-tab">
                     <div class="col-lg-6 col-sm-12 col-md-6">
                        <div class="portfolio_box">
                           <a href="{{ asset('/dreams-master/img/sekolah_2.png')}}" class="img-gal">
                              <div class="single_portfolio">
                                 <img class="img-fluid w-100" src="{{ asset('/dreams-master/img/sekolah_2.png')}}" alt="">
                              </div>
                           </a>
                           <div class="short_info">
                              <h4>Seragam</h4>
                              <p>Siswa Putra</p>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-5 col-md-6">
                        <div class="row">
                           <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                              <div class="portfolio_box">
                                 <a href="{{ asset('/dreams-master/img/project-2.png')}}" class="img-gal">
                                    <div class="single_portfolio">
                                       <img class="img-fluid w-100" src="{{ asset('/dreams-master/img/project-2.png')}}" alt="">
                                    </div>
                                 </a>
                                 <div class="short_info">
                                 <h4>Rekayasa Perangkat Lunak</h4>
                                  <p>Rekayasa Perangkat Lunak adalah <br>
                                 Jurusan yang berada di bidang komputer</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                              <div class="portfolio_box">
                                 <a href="{{ asset('/dreams-master/img/project-3.png')}}" class="img-gal">
                                    <div class="single_portfolio">
                                       <img class="img-fluid w-100" src="{{ asset('/dreams-master/img/project-3.png')}}" alt="">
                                    </div>
                                 </a>
                                 <div class="short_info">
                                 <h4>Teknik Kendaraan Ringan Otomotif</h4>
                                  <p>Teknik Kendaraan Ringan Otomotif adalah <br>
                                 Jurusan yang berada di bidang Otomotif</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="row align-items-center justify-content-between tab-pane fade" id="apph" role="tabpanel"
                     aria-labelledby="apph-tab">
                     <div class="col-lg-6 col-sm-12 col-md-6">
                        <div class="portfolio_box">
                           <a href="{{ asset('/dreams-masterimg/seragam_1.png')}}" class="img-gal">
                              <div class="single_portfolio">
                                 <img class="img-fluid w-100" src="{{ asset('/dreams-master/img/seragam_1.png')}}" alt="">
                              </div>
                           </a>
                           <div class="short_info">
                           <h4>Seragam</h4>
                              <p>Siswi Putri</p>
                              </div>
                        </div>
                     </div>
                     <div class="col-lg-5 col-md-6">
                        <div class="row">
                           <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                              <div class="portfolio_box">
                                 <a href="{{ asset('/dreams-master/img/ap.png')}}" class="img-gal">
                                    <div class="single_portfolio">
                                       <img class="img-fluid w-100" src="{{ asset('/dreams-master/img/ap.png')}}" alt="">
                                    </div>
                                 </a>
                                 <div class="short_info">
                                 <h4>Administrasi Perkantoran</h4>
                                  <p> Administrasi Perkantoran adalah <br>
                                 Jurusan yang berada di bidang Perkantoran</p>
                                    </div>
                              </div>
                           </div>
                           <div class="col-lg-12 col-sm-6 col-md-12 single_portfolio_project">
                              <div class="portfolio_box">
                                 <a href="{{ asset('/dreams-master/img/ph.png')}}" class="img-gal">
                                    <div class="single_portfolio">
                                       <img class="img-fluid w-100" src="{{ asset('/dreams-master/img/ph.png')}}" alt="">
                                    </div>
                                 </a>
                                 <div class="short_info">
                                 <h4>Perhotelan</h4>
                                  <p> Perhotelan adalah <br>
                                    Jurusan yang berada di bidang Perhotelan</p>
                                    </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
    </div>
   </div>
   </section>
   <!--::project part end::-->

   <!--::service part end::-->
   <section class="service_part">
      <div class="container">
         <div class="row justify-content-between align-items-center">
            <div class="col-lg-7 col-xl-6">
               <div class="section_tittle">
                  <h2>Falisitas <span>Sekolah</span></h2>
               </div>
               <div class="service_part_iner">
                  <div class="row">
                     <div class="col-lg-6 col-sm-6">
                        <div class="single_service_text ">
                           <img src="{{ asset('/dreams-master/img/icon/service_1.svg')}}" alt="">
                           <h4>Lab Komputer</h4>
                           <p>Sekolah SMKN 1 CISARUA mempunyai Lab Komputer yang cukup banyak </p>
                        </div>
                     </div>
                     <div class="col-lg-6 col-sm-6">
                        <div class="single_service_text">
                           <img src="{{ asset('/dreams-master/img/icon/service_2.svg')}}" alt="">
                           <h4>Perpusta
                              kaan</h4>
                           <p>Sekolah SMKN 1 CISARUA mempunyai Perpustakan yang luas dan mempunyai buku yang banyak </p>
                        </div>
                     </div>
                     <div class="col-lg-6 col-sm-6">
                        <div class="single_service_text">
                           <img src="{{ asset('/dreams-master/img/icon/service_3.svg')}}" alt="">
                           <h4>Masjid</h4>
                           <p>Jika anda yang muslim dan bersekolah di SMKN 1 CISARUA kalian bisa menggunakan fasilitas yang satu ini</p>
                        </div>
                     </div>
                     <div class="col-lg-6 col-sm-6">
                        <div class="single_service_text">
                           <img src="{{ asset('/dreams-master/img/icon/service_4.svg')}}" alt="">
                           <h4>Lapangan</h4>
                           <p>Jika kalian suka dengan yang namanya olahraga kalian bisa menggunakan lapangan tersebut buat kalian berolahraga</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-lg-4 col-sm-10">
               <div class="service_text">
                  <h2>Hallo Kami Memiliki Beberapa Fasilitas Sekolah <span> Lohh.</span></h2>
                  <p>SMKN 1 CISARUA memiliki banyak fasilitas diantaranya yang sudah kalian bisa 
                     lihat di sebelah kiri itu hanya segelintir fasilitas masih banyak fasilitas 
                     selain yang sudah di sebutkan. Jika kalian ingin mengetahui apa saja datang lah
                      ke sekolah kami dan kalian bersekolah lah disini</p>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!--::service part end::-->

  