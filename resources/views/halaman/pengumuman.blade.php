@extends('layouts.home')
@section('content')
<!--::breadcrumb part start::-->

<div id="mycarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="item active">
        <img src="{{ asset('/dreams-master/img/sekolah_5.jpg')}}" alt="" class="img-responsive" width="1500" height="500">
           <div class="carousel-caption">
         
      <h1><mark>Pengumuman</mark></h1>
         </div>
      </div>
    </div>
</div>



        <table class="table">
            <thead class="thead-light">
              <tr>
                
                <th scope="col">Judul</th>
                <th scope="col">Link Document</th>
                <th scope="col">Tanggal</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pengumuman as $key=>$value)
                    <tr>
                        
                        <td>{{$value->judul}}</td>
                        <td><a href="{{$value->isi}}"class="btn btn-primary">{{$value->isi}}</a></td>
                        <td>{{$value->tanggal}}</td>
                        <td>
                          
                           
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
                  
             

   <!--::card box end::-->
@endsection