@extends('layouts.master')
@section('judul')
Tambah Data
@endsection
@section('content')
<div>
        <form action="/pengumuman" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" name="judul" id="title" placeholder="Masukkan Judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Isi</label>
                <input type="text" class="form-control" name="isi" id="body" placeholder="isi">
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                <div class="form-group">
                <label for="body">Isi</label>
                <input type="date" class="form-control" name="tanggal" id="body" placeholder="date">
                @error('tanggal')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection