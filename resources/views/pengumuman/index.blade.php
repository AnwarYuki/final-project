@extends('layouts.master')

@section('judul')
View
@endsection
@section('content')
<a href="/pengumuman/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Isi</th>
                <th scope="col">Tanggal</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($pengumuman as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{$value->isi}}</td>
                        <td>{{$value->tanggal}}</td>
                        <td>
                            <a href="/pengumuman/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/pengumuman/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/pengumuman/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection