<footer class="footer_part">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
            </div>

            <div class="col-sm-6 col-lg-3">
               <div class="single_footer_part">
                  <h4>About Us</h4>
                  <p>Sekolah SMKN 1 CISARUA adalah SMK yang terletak di Kecamatan Cisarua Kabupaten Bandung Barat bukan di bogor yaa 
                     karena emang Cisarua itu ada 2 ada yang di daerah Bandung sama Bogor </p>
               </div>
            </div>
            <div class="col-sm-6 col-lg-3">
               <div class="single_footer_part">
                  <h4>Contact Info</h4>
                  <p>Address : Jalan Kolonel Masturi Desa Jambudipa</p>
                  <p>Phone : 083120371009</p>
                  <p>Email : senibudayasmkn1cisarua@gmail.com</p>
               </div>
            </div>
            <div class="col-sm-6 col-lg-3">
               <div class="single_footer_part">
                  <h4>Jurusan</h4>
                  <p>Rekayasa Perangkat Lunak</p>
                  <p>Administrasi Perkantoran</p>
                  <p>Perhotelan</p>
                  <p>Teknik Kendaraan Ringan Otomotif</p>
               </div>
            </div>
         </div>
         <hr>
         <div class="row">
            <div class="col-lg-12">
               <div class="copyright_text text-center">
                  <P><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved || <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">AL</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></P>
               </div>
            </div>
         </div>
      </div>
   </footer>