<header class="main_menu home_menu">
      <div class="container">
         <div class="row">
            <div class="col-lg-12">
               <nav class="navbar navbar-expand-lg navbar-light">
                  <a class="navbar-brand" href="index.html"> <img src="{{ asset('/dreams-master/img/logo.png')}}" alt="logo"> </a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                     aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                     <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse main-menu-item" id="navbarNav">
                     <ul class="navbar-nav">
                        <li class="nav-item active">
                           <a class="nav-link" href="/">Dashboard</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/halabout">About</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/halpengumuman">Pengumuman</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/halevent">Event</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/haldataguru">Data Guru</a>
                        </li>
                        <li class="nav-item">
                           <a class="nav-link" href="/halcontact">Contact us</a>
                        </li>
                     </ul>
                  </div>
               </nav>
            </div>
         </div>
      </div>
   </header>