@extends('layouts.master')

@section('judul')
Edit Data 
@endsection
@section('content')
<div>
        <form action="/dataguru/{{$dataguru->id}}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" value="{{$dataguru->nama}}" name="nama" id="title" placeholder="Edit Judul">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="body">Mata Pelajaran</label>
                <input type="text" class="form-control" value="{{$dataguru->matapelajaran}}" name="matapelajaran" id="body" placeholder="Edit Isi">
                @error('matapelajaran')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
@endsection