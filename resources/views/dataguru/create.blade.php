@extends('layouts.master')

@section('judul')
Tambah Data
@endsection
@section('content')
<div>
        <form action="/dataguru" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Nama</label>
                <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Nama">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
            <select name ="matapelajaran" class="form-control"  id="body" placeholder="Mata Pelajaran">
                <option value="Matematika">Mataematika</option>
                <option value="Bahasa Indonesia">Bahasa Indonesia</option>
                <option value="Bahasa Inggris">Bahasa Inggris </option>
                </select>
                @error('matapelajaran')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
@endsection