@extends('layouts.master')

@section('judul')
View
@endsection
@section('content')
<a href="/dataguru/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Mata Pelajaran</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($dataguru as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->matapelajaran}}</td>
                        <td>
                            <a href="/dataguru/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/dataguru/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/dataguru/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection