<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('halaman.dashboard');
});
Route::get('/halpengumuman', 'HalamanController@index');
Route::get('/halevent', 'HalamanController@event');
Route::get('/haldataguru', 'HalamanController@dataguru');
Route::get('/halcontact', 'HalamanController@contact');
Route::get('/halabout', 'HalamanController@about');


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//crud 
Route::resource('pengumuman', 'PengumumanController');
Route::resource('event', 'EventController');
Route::resource('dataguru', 'DataguruController');
